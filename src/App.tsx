import React from "react";
import Img from "./assets/image/logo.png";

const App = () => {
  return (
    <div className="container">
      <div>
        <div className="title">
          <h1>Webpack</h1>
          <h1 className="black">course</h1>
        </div>
        <img src={Img} alt="logo" />
      </div>
    </div>
  );
};

export default App;
